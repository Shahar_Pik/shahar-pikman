__author__ = 'Shahar'

import socket
import re
import os
from PIL import ImageGrab  # My extra import (Asaf)

#Constants
IP = '127.0.0.1'
PORT = 80
DATA_SIZE = 1024
LISTENERS = 1
CONNECT_AGAIN = True
GET_STRUCTURE = r'GET (/\S+) HTTP/1\.1\r\n\S+'
NEXT_STRUCT = r'/calculate-next\?num=(-*\d+)'
AREA_STRUCT = r'/calculate-area\?height=(\d+\.?\d*)&width=(\d+\.?\d*)'
HEADERS_STRUCT = 'HTTP\\1.0 %i %s \r\nContent-Length: %i\r\n\r\n%s'
POST_STRUCTURE = r'POST /(\S+)\?file-name=(\S+) HTTP/1\.1\r\n'
IMAGE_STRUCT = r'/image\?image-name=(\S+)'
ROOT_DIR = 'C:\wwwroot'
SAVED_PHOTO = 'C:\wwwroot\print_screen_file.jpg'
OK = 200
TEMP_MOVED = 302
FORBIDDEN = 403
NOT_FOUND = 404
SERVER_ERROR = 500

PRINT_STRUCY = r'/print-screen'  # My extra structure (Asaf)


def open_conn():
    """
    Get: ---
    Do: Creates a new socket and opening a connection
    Return: the server and the connection
    """
    global server_s
    server_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_s.bind((IP, PORT))
    server_s.listen(LISTENERS)
    global conn
    conn, address = server_s.accept()
    return conn, server_s


def answers():
    """
    Get: ---
    Do: receiving the GET request, and sending the correct answers for her
    Return: ---
    """
    moved_files = [r'C:\wwwroot\page1.html']
    request = conn.recv(DATA_SIZE)
    match = re.search(GET_STRUCTURE, request)
    if not match:
        match = re.search(POST_STRUCTURE, request)
        if not match:
            conn.send(build_packet(SERVER_ERROR))
        save_pic(match.group(2))
    else:
        if r'/calculate-next' in match.group(1):
            conn.send(calculate_next(match.group(1)))
        if r'/calculate-area' in match.group(1):
            conn.send(triangle_area(match.group(1)))
        if r'/print-screen' in match.group(1):
            conn.send(print_screen(match.group(1)))
        if r'/image' in match.group(1):
            conn.send(print_image(match.group(1)))

        file_path = ROOT_DIR + match.group(1)
        if os.path.isfile(file_path):
            if os.access(file_path, os.R_OK):
                if file_path in moved_files:
                    build_packet(TEMP_MOVED, file_path)
                with open(file_path, 'r') as file_name:
                    text = file_name.read()
                    conn.send(text)
                    answer_headers = build_packet(OK)
                    conn.send(answer_headers)
            else:
                conn.send(build_packet(FORBIDDEN))
        else:
            conn.send(build_packet(NOT_FOUND))


def build_packet(status_code, data=None):
    """
    Get: the code of the suitable error/success message, data (in case of a moved file or calculator
    Do: create the headers
    Return: the correct headers
    """
    codes_dict = {
        OK: 'OK',
        FORBIDDEN: 'Forbidden',
        NOT_FOUND: 'Not Found',
        TEMP_MOVED: 'Moved Temporarily',
        SERVER_ERROR: 'Internal Server Error'
    }

    moved_files = {
        r'C:\wwwroot\page1.html': r'C:\wwwroot\page2.html'
    }

    if data in moved_files:
        desc = '%s moved to %s' % (codes_dict[status_code], moved_files[data])
        headers = HEADERS_STRUCT % (status_code, codes_dict[status_code], len(codes_dict[status_code] + desc), desc)
    elif data:
        headers = HEADERS_STRUCT % (status_code, codes_dict[status_code], len(data), data)
    else:
        headers = HEADERS_STRUCT %\
            (status_code, codes_dict[status_code], len(codes_dict[status_code]), codes_dict[status_code])
    return headers


def print_screen(request):  # My extra function
    """
    Get: the path part in the GET request
    Do: creates a print screen
    Return: 404 packet(if something else then '/print-screen' is written in the url) or a 200 packet with the data
            (a print screen)
    """

    if request == PRINT_STRUCY:
        return build_packet(NOT_FOUND)
    img = ImageGrab.grab()
    img_path = SAVED_PHOTO
    img.save(img_path)
    with open(img_path, 'rb') as screen_shot_file:
        screen_shoot = screen_shot_file.read()
    os.remove(img_path)
    answer_headers = build_packet(OK, screen_shoot)
    return answer_headers


def calculate_next(request):
    """
    Get: the path part in the GET request
    Do: finds the parameter itself (an int) in the path, and calculating the number after it (the sequential number)
    Return: 404 packet (if the parameters aren't correct) or a 200 packet with the data (the sequential number)
    """
    match = re.search(NEXT_STRUCT, request)
    if not match:
        return build_packet(NOT_FOUND)
    num = str(int(match.group(1)) + 1)
    answer_headers = build_packet(OK, num)
    return answer_headers


def triangle_area(request):
    """
    Get: the path part in the GET requests
    Do: finds the parameter themselves (two floats - the height and the width of a triangle) in the path.
        Then, the function is calculating the area of the triangle.
    Return: 404 packet (if the parameters aren't correct) or a 200 packet with the data (the area of the triangle)
    """
    match = re.search(AREA_STRUCT, request)
    if not match:
        return build_packet(NOT_FOUND)
    height = float(match.group(1))
    width = float(match.group(2))
    area = (height * width) / 2
    answer_headers = build_packet(OK, str(area))
    return answer_headers


def save_pic(file_name):
    """
    Get: The file name
    Do: Saves the picture to the wwwroot directory
    Return: ---
    """
    dest_path = '%s\%s.jpg' % (ROOT_DIR, file_name)
    pic = open(dest_path, 'wb')
    data = conn.recv(1024)
    while data != 'end':
        pic.write(data)
        data = conn.recv(1024)
    pic.close()


def print_image(request):
    """
    Get: the file name
    Do: opens the picture in the web browser
    Return: 404 packet (if the parameters aren't correct) or a 200 packet with the data (the picture itself)
    """
    match = re.search(IMAGE_STRUCT, request)
    if not match:
        return build_packet(NOT_FOUND)
    img_path = '%s\%s' % (ROOT_DIR, match.group(1))
    with open(img_path, 'rb') as pic:
        data = pic.read()
    answer_headers = build_packet(OK, data)
    return answer_headers


def main():
    open_conn()
    if not os.path.isdir(ROOT_DIR):
        os.mkdir(ROOT_DIR)
    while CONNECT_AGAIN:
        answers()
        conn.close()
        server_s.close()
        open_conn()

if __name__ == "__main__":
    main()